package uz.test.jerseyapp.repository;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.jdbc.core.JdbcTemplate;
import uz.test.jerseyapp.entity.Todo;
import uz.test.jerseyapp.utill.Tools;

import java.sql.ResultSet;
import java.util.List;


@RequiredArgsConstructor
public class TodoRepository {
    JdbcTemplate jdbcTemplate = Tools.getJdbcTemplate();

    public void add(Todo todo) {
        jdbcTemplate.update(
                "insert into todo(title,description,has_completed,deadline) values (?,?,?,?)",
                todo.getTitle(),
                todo.getDescription(),
                todo.getHasCompleted(),
                todo.getDeadline()
        );
    }

    public List<Todo> get() {
        return jdbcTemplate.query("select * from todo",
                (rs, rowNum) -> getTodoFromResultSet(rs));
    }


    public Todo get(Integer id) {
        return jdbcTemplate.queryForObject("select * from todo where id = ?",
                new Object[]{id},
                (rs, rowNum) -> getTodoFromResultSet(rs));
    }

    public void edit(Integer id, Todo todo) {
        jdbcTemplate.update("update todo set  title=?, description=?, has_completed=?, deadline=? where id = ?",
                todo.getTitle(),
                todo.getDescription(),
                todo.getHasCompleted(),
                todo.getDeadline(),
                id
        );
    }

    public void delete(Integer id) {
        jdbcTemplate.update("delete from todo where id = ?", id);
    }

    @SneakyThrows
    private Todo getTodoFromResultSet(ResultSet rs) {
        return new Todo(
                rs.getInt("id"),
                rs.getString("title"),
                rs.getString("description"),
                rs.getBoolean("has_completed"),
                rs.getTimestamp("deadline")
        );
    }
    private static TodoRepository instance;
    public static TodoRepository getInstance() {
        if (instance == null) {
            instance=new TodoRepository();
        }
        return instance;
    }
}
