package uz.test.jerseyapp.controller;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.Data;
import lombok.SneakyThrows;
import uz.test.jerseyapp.entity.Todo;
import uz.test.jerseyapp.repository.TodoRepository;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;


@Path("/todos")
public class TodoController {


    TodoRepository repository = TodoRepository.getInstance();
//    URI uri = new URI("localhost:8080/api/todos");

    public TodoController()  {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTodos() {
//        return Response.ok(repository.get()).build();
        Response.ResponseBuilder responseBuilder = Response.ok(repository.get());
        responseBuilder.header("Access-Control-Allow-Origin", "*"); // Replace "*" with the actual allowed origin if necessary

        return responseBuilder.build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response getTodos(@PathParam("id") int id) {

        return Response.ok(repository.get(id)).build();
    }

    @SneakyThrows
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addTodo(Todo todo) {
        repository.add(todo);
        return Response.status(Response.Status.CREATED).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response update(@PathParam("id") int id, Todo todo) {
        repository.edit(id, todo);
        return Response.ok(true).build();
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") int id) {
        repository.delete(id);
        return Response.ok(true).build();
    }

}

