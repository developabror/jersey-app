package uz.test.jerseyapp.utill;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class Tools {
    private static JdbcTemplate jdbcTemplate;
    public static JdbcTemplate getJdbcTemplate(){
        if (jdbcTemplate == null){
            jdbcTemplate = new Tools().jdbcTemplate();
        }
        return jdbcTemplate;
    }
    public DriverManagerDataSource driverManagerDataSource() {
        DriverManagerDataSource driverManagerDataSource =
                new DriverManagerDataSource(
                        "jdbc:postgresql://localhost:5432/test",
                        "postgres",
                        "root123");
        driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
        return driverManagerDataSource;
    }

    public JdbcTemplate jdbcTemplate() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(driverManagerDataSource());
        try {

        jdbcTemplate.update("create table todo(id serial primary key,title varchar,description varchar,has_completed bool,deadline timestamp)");
        }catch (Exception e) {}
        return jdbcTemplate;
    }
}
