package uz.test.jerseyapp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Todo {
    private Integer id;
    private String title;
    private String description;
    private Boolean hasCompleted;
    private Timestamp deadline;
}
/*
create table todo(
id serial primary key,
title varchar,
description varchar,
has_completed bool,
deadline timestamp
);


*/